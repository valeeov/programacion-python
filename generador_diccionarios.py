import itertools

lista_diccionario = []

def readList(archivo):
	#Abrir el archivo de la lista de usuarios
        with open(archivo, 'r') as lista:
        #Leer la lista, y strip nos ayudara a borrar \n
            return [cadena.strip('\n')for cadena in lista.readlines()]

def hacer_permutaciones(lista):
	#Por cada palabra realiza 1 a 3 combinaciones de la palabras restantes
	for l in range(1,4):
		#itertools.permutations = metodo que realiza las permutaciones
		#pasar los elementos permutados a una lista
    		lista1 = list(itertools.permutations(lista,l))
		#Recorre permutacion x permutacion
		for l2 in lista1:
			#Convertir lista a cadena
			lista2 = " ".join(l2)
			lista_diccionario.append(lista2)

def convertir_mayusculas(lista):
	with open ('diccionarios', 'a') as lista_mayusculas:
		for p in lista:
			lista_mayusculas.write(p.upper()+ '\n')

def convertir_minusculas(lista):
	with open ('diccionarios', 'a') as lista_minusculas:
		for p in lista:
			lista_minusculas.write(p.lower()+'\n')

def convertir_capitalize(lista):
	with open('diccionario','a') as lista_capitalize:
		for p in lista:
			lista_capitalize.write(p.capitalize()+'\n') 

def replace_cadenas(lista):
	with open ('diccionario','a') as lista_replace:
		for p in lista:
			lista_replace.write(p.replace('a','4').replace('e','3').replace('i','1').replace('o','0')+'\n')


regresar_palabras = readList('generador_palabras')
hacer_permutaciones (regresar_palabras)
convertir_mayusculas(lista_diccionario)
convertir_minusculas(lista_diccionario)
convertir_capitalize(lista_diccionario)
replace_cadenas(lista_diccionario)
