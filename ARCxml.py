import sys 
import xml.etree.ElementTree as ET 
from datetime import datetime 

hosts = []
arc_hosts = []

class host:
    def __init__(self,statusHost,addr,port22,port53,port80,port443,nameDom,serviceHTTP,serviceHTTPS):
        self.statusHost = statusHost
        self.addr = addr
        self.port22 = port22
        self.port53 = port53
        self.port80 = port80
        self.port443 = port443
        #self.statusPort =statusPort
        self.nameDom = nameDom
        self.serviceHTTP = serviceHTTP
        self.serviceHTTPS = serviceHTTPS
    
    def __str__(self):
        return '%s,%s,%s,%s,%s,%s,%s,%s,%s' % (self.statusHost,
					      self.addr,
					      self.port22,
					      self.port53,
					      self.port80,
					      self.port443,
					      self.nameDom,
					      self.serviceHTTP,
					      self.serviceHTTPS)

def printError(msg, exit = False):
    sys.stderr.write('Error:\t%s\n' % msg)
    if exit:
        sys.exit(1)


def lee_xml(nmap):
    eCont22 = 0
    eCont53 = 0
    eCont80 = 0
    eCont443 = 0
    
    with open (nmap,'r') as nmap:
        root = ET.fromstring(nmap.read())
        for h in root.findall('host'):
            statusHost = h.find('status').get('state')
            addr = h.find('address').get('addr')
            for hostname in h.findall('hostnames'):
                try:
                    nameDom = hostname.find('hostname').get('name')
                except:
                    nameDom = ''
                for p in h.findall('ports'):
                    for pp in p.findall('port'):
                        if pp.get('portid')=='22':
                            if  pp.find('state').get('state')=='open':
                                print "puerto22"
                                port22 = pp.get('portid')
                                eCont22 += 1
                            else:
                                port22=''
                        elif pp.get('portid')=='53':
                            if pp.find('state').get('state')=='open':
                                port53 = pp.get('portid')
                                eCont53 += 1
                            else:
                                port53=''
                        elif pp.get('portid')=='80':
                            if pp.find('state').get('state')=='open':
                                port80 = pp.get('portid')
                                eCont80 += 1
                                serviceHTTP = pp.find('service').get('product')
                            else:
                                port80=''
                                serviceHTTP= ''
                        elif pp.get('portid')=='443':
                            if  pp.find('state').get('state')=='open':
                                port443 = pp.get('portid')
                                eCont443 += 1
                                serviceHTTPS = pp.find('service').get('product')
                            else:
                                port443=''
                                serviceHTTPS = ''
                hosts = host(statusHost,addr,port22,port53,port80,port443,nameDom,serviceHTTP,serviceHTTPS)
                arc_hosts.append(hosts)

#Recorrer arc_hosts
#For i in arc_hoss
	#if i.statusHost  ==  'up'
		#eContUP += 1
		#if i.port22 == '22'
			#eCont22 +=1
	#else
	   #eContDOWN += 1

def contarPuertos():
    statusHost = 0
    eContUp = 0
    port22 = 0
    eCont22Up = 0
    eCont22Down = 0
    port53 = 0
    eCont53Up = 0
    eCont53Down = 0
    port80 = 0
    eCont80Up = 0
    eCont80Down = 0
    port443 = 0
    eCont443Up = 0
    eCont443Down = 0
    eCountDown = 0
    eContHoneypot = 0
    eContApache = 0
    eContNginx = 0
    
    for i in arc_hosts:
        if i.statusHost == 'up':
            eContUp += 1
            if i.port22 == '22':
                eCont22Up += 1
            else:
                eCont22Down += 1
            if i.port53 == '53':
                eCont53Up += 1
            else:
                eCont53Down += 1
            if i.port80 == '80':
                eCont80Up += 1
            else:
                eCont80Down += 1
            if i.port443 == '443':
                eCont443Up += 1
            else:
                eCont443Down += 1
            if i.serviceHTTP == 'nginx':
                eContNginx += 1
            elif i.serviceHTTP ==  'Apache httpd':
                eContApache += 1
            elif i.serviceHTTP == 'Dionaea Honeypot httpd':
                eContHoneypot += 1 
            else:
                print "holi"
                eCountDown += 1
    
    print "Host prendidos", eContUp
    print "22Up:",eCont22Up
    print "22Down: ",eCont22Down
    print "53Up", eCont53Up
    print "53Down",eCont53Down
    print "80Up",eCont80Up
    print "80Down",eCont80Down
    print "443Up",eCont443Up
    print "443Down",eCont443Down
    print "Host apagados", eCountDown
    print "Honeypot", eContHoneypot 
    print "Apache", eContApache
    print "ngnix", eContNginx
	


def escribe_reporte(archivo_reporte):
    with open(archivo_reporte,'w') as output:
        output.write(str(datetime.now()) + '\n')
        map(lambda u: output.write(str(u)+'\n'),arc_hosts)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        printError('Indicar archivo a leer y archivo de reporte.', True)
    lee_xml(sys.argv[1])
    escribe_reporte(sys.argv[2])
    contarPuertos()
