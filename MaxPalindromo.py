#!/usr/bin/python
# -*- coding: utf-8 -*-

#Se guarda la lista de palindromos que se encontrarán en la cadena
lista_palindromos = []

def palindromoMax(cadena):

	#Recorre todas las letras de la cadena (Inicio-Fin)
	for letra in range(0,len(cadena)+1):
		fin = len(cadena) -1
		#Recorre todas de la cadena (Fin-Inicio)
		for letra_fin in range(fin,-1,-1):
				#Validar que la cadena de inicio coincida con la cadena fin 
				#Se agrega el +1  porque es excluyente
				validar_cadena = cadena[letra:letra_fin + 1]
				#Validamos que la cadena sea igual a la cadena inversa
				if (validar_cadena == validar_cadena[::-1]):
					#Se valida que los valores que se hayan se encontrado sean mayores a 1
					if (len(validar_cadena)>1):
						print validar_cadena
						lista_palindromos.append(validar_cadena)


	#Regresa el palindromo máximo
	print lista_palindromos
	#Key:Aplica la funcion len por cada palindromo encontrado en lista_palindromos 
	return max(lista_palindromos, key = len)


cadena = palindromoMax('Holacomoestasanitalavalatina')
print cadena
