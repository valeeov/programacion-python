from random import choice 
digitos = ('a','b','c','d','e','f','g','h',
'i','j','k','l','m','n','o','p','q','r','s',
't','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','L','M','O','P',
'Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9','*',',','.')

def asigna_contrasena(long):
    #Se asigna una cadena vacía para guardar los valores 
	contrasena = ''
	if long == 15:
	    #Regresa de forma aleatoria digitos hasta llegar a 15 
		return choice(digitos)
	else:
	   #Si no llega a 15, sigue asignando numeros aleatorios y asigna contrasena se agrega en 1 
		contrasena += choice(digitos) + asigna_contrasena(long + 1)
    
	return contrasena

#Empieza con el 0 hasta llegar a los 15 digitos 
print asigna_contrasena(0)

