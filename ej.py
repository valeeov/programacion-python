#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT

aprobados = []

def aprueba_becario(nombre_completo):
    nombre_separado = nombre_completo.split()
    for n in nombre_separado:
	valida_nombre = n[0].upper() + n[1:].lower()
        if valida_nombre in ['Manuel', 'Valeria', 'Alejandro', 'Luis', 'Enrique','Omar','Abraham','Oscar']:
            return False
    aprobados.append(nombre_completo.upper()) #Método para agregar una lista y .upper para hacer el MAYUSCULAS  
    aprobados.sort() #Método de ordenamiento 
    
    return True

def elimina_becario(nombre_completo):
    #Pasar a la variable los nombres en mayusculas 
    nombre_mayusculas = nombre_completo.upper() 
    #Si te encuentras en la lista de aprobados 
    if nombre_mayusculas in aprobados:
    #Con el método pop elimina
	if (aprobados.pop(nombre_mayusculas)):
		return true 
	else:
		return false 


becarios = ['Cervantes Varela JUAN MaNuEl',
            'Leal GonzÃ¡lez IgnaciO',
            'Ortiz Velarde valeria',
            'MartÃ­nez Salazar LUIS ANTONIO',
            'RodrÃ­guez Gallardo pedro alejandro',
            'Tadeo GuillÃ©n DiAnA GuAdAlUpE',
            'Ferrusca Ortiz jorge luis',
            'JuÃ¡rez MÃ©ndez JeSiKa',
            'Pacheco Franco jesus ENRIQUE',
            'Vallejo FernÃ¡ndez RAFAEL alejanDrO',
            'LÃ³pez FernÃ¡ndez serVANDO MIGuel',
            'HernÃ¡ndez GonzÃ¡lez ricaRDO OMAr',
            'Acevedo GÃ³mez LAura patrICIA',
            'Manzano Cruz isaÃ­as AbrahaM',
            'Espinosa Curiel OscaR']
for b in becarios:
    if aprueba_becario(b):
        print 'APROBADOO: ' + b
    else:
        print 'REPROBADO: ' + b


#print becarios


aprueba_becario ('Oscar') 
elimina_becario ('Alejandro') 

